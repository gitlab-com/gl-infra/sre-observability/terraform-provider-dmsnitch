{
  description = "Elusive and forbidden knowledge!";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/f28c957a927b0d23636123850f7ec15bda9aa2f4";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = {self, nixpkgs, flake-utils, ...}: 
    flake-utils.lib.eachDefaultSystem (system:
      let 
        pkgs = import nixpkgs { inherit system; };
      in {
        packages = {};
        devShell = (pkgs.callPackage ./shell.nix {});
      });
}
