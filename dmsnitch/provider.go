package dmsnitch

import (
	"context"
	"net/http"
	"os"

	"github.com/hashicorp/terraform-plugin-framework/datasource"
	"github.com/hashicorp/terraform-plugin-framework/path"
	"github.com/hashicorp/terraform-plugin-framework/provider"
	"github.com/hashicorp/terraform-plugin-framework/provider/schema"
	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-framework/types"
)

// Ensure the implementation satisfies the expected interfaces
var (
	_ provider.Provider = &dmsnitchProvider{}
)

// New is a helper function to simplify provider server and testing implementation.
func New() provider.Provider {
	return &dmsnitchProvider{}
}

// dmsnitchProvider is the provider implementation.
type dmsnitchProvider struct{}

type dmsnitchProviderModel struct {
	ApiKey types.String `tfsdk:"api_key"`
}

func (p *dmsnitchProvider) Schema(_ context.Context, _ provider.SchemaRequest, resp *provider.SchemaResponse) {
	resp.Schema = schema.Schema{
		Attributes: map[string]schema.Attribute{
			"api_key": schema.StringAttribute{
				Optional:  true,
				Sensitive: true,
			},

			// More attributes here
		},
	}
}

func (p *dmsnitchProvider) Configure(ctx context.Context, req provider.ConfigureRequest, resp *provider.ConfigureResponse) {
	var config dmsnitchProviderModel
	diags := req.Config.Get(ctx, &config)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	if config.ApiKey.IsUnknown() {
		resp.Diagnostics.AddAttributeError(
			path.Root("api_key"),
			"Unknown DMS Api Key",
			"The provider cannot create the DMS API client as there is an unknown value for the DMS API Key."+
				"Either target apply the source of the value first, set the value statically in the configuration, or use the DMSNITCH_API_KEY environment variable.",
		)
	}

	if resp.Diagnostics.HasError() {
		return
	}

	apikey := os.Getenv("DMSNITCH_API_KEY")

	if !config.ApiKey.IsNull() {
		apikey = config.ApiKey.ValueString()
	}

	if apikey == "" {
		resp.Diagnostics.AddAttributeError(
			path.Root("api_key"),
			"Missing DMS Api Key",
			"The provider cannot create the DMS API client as there is a missing value for the DMS API Key."+
				"Either target apply the source of the value first, set the value statically in the configuration, or use the DMSNITCH_API_KEY environment variable.",
		)
	}

	if resp.Diagnostics.HasError() {
		return
	}
	client := &DMSnitchClient{
		ApiKey:     apikey,
		HTTPClient: &http.Client{},
	}

	resp.DataSourceData = client
	resp.ResourceData = client
}

// Metadata returns the provider type name.
func (p *dmsnitchProvider) Metadata(_ context.Context, _ provider.MetadataRequest, resp *provider.MetadataResponse) {
	resp.TypeName = "dmsnitch"
}

// DataSources defines the data sources implemented in the provider.
func (p *dmsnitchProvider) DataSources(_ context.Context) []func() datasource.DataSource {
	return []func() datasource.DataSource {
		NewSnitchDataSource,
	}
}

// Resources defines the resources implemented in the provider.
func (p *dmsnitchProvider) Resources(_ context.Context) []func() resource.Resource {
	return []func() resource.Resource{
		NewSnitchResource,
	}
}
