package dmsnitch

import (
	"fmt"
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"

	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-log/tflog"
)

// Create creates the resource and sets the initial Terraform state.
func (r *snitchResource) Create(ctx context.Context, req resource.CreateRequest, resp *resource.CreateResponse) {
	// Retrieve values from plan
	var plan snitchResourceModel
	diags := req.Plan.Get(ctx, &plan)

	tflog.Debug(ctx, fmt.Sprintf("DMS/CreateSnitch-Plan: %+v", plan))

	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	bytedata, err := json.Marshal(&plan)
	if err != nil {
		resp.Diagnostics.AddError(
			"Error creating snitch",
			"Could not create snitch, unexpected error: "+err.Error(),
		)
		return
	}

	dresp, err := r.client.Post("snitches", bytes.NewBuffer(bytedata))
	if err != nil {
		resp.Diagnostics.AddError(
			"Error creating snitch",
			"Could not create snitch, unexpected error: "+err.Error(),
		)
		return
	}

	body, err := ioutil.ReadAll(dresp.Body)
	if err != nil {
		resp.Diagnostics.AddError(
			"Error creating snitch",
			"Could not read response from DMS api, unexpected error: "+err.Error(),
		)
		return
	}

	err = json.Unmarshal(body, &plan)
	if err != nil {
		resp.Diagnostics.AddError(
			"Error creating snitch",
			"Could not read response from DMS api, unexpected error: "+err.Error(),
		)
		return
	}

	// Set the state.
	diags = resp.State.Set(ctx, plan)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}
}
