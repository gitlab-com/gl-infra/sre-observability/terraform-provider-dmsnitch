package dmsnitch

import (
	"context"
	//"log"
	"encoding/json"

	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema/stringplanmodifier"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema/planmodifier"
	"github.com/hashicorp/terraform-plugin-log/tflog"
)

// Ensure the implementation satisfies the expected interfaces.
var (
	_ resource.Resource                = &snitchResource{}
	_ resource.ResourceWithConfigure   = &snitchResource{}
	_ resource.ResourceWithImportState = &snitchResource{}
)

// NewSnitchResource is a helper function to simplify the provider implementation.
func NewSnitchResource() resource.Resource {
	return &snitchResource{}
}

// snitchResource is the resource implementation.
type snitchResource struct {
	client *DMSnitchClient
}

type snitchResourceModel struct {
	Id       types.String   `tfsdk:"id"`
	Href      types.String   `tfsdk:"href"`
	Name     types.String   `tfsdk:"name"`
	Interval types.String   `tfsdk:"interval"`
	Type     types.String   `tfsdk:"alert_type"`
	Email    []types.String `tfsdk:"alert_email"`
	Notes    types.String   `tfsdk:"notes"`
	Tags     []types.String `tfsdk:"tags"`
}

func (m *snitchResourceModel) UnmarshalJSON(data []byte) error {
	type Alias struct {
		Token  string `json:"token,omitempty"`
		Href    string `json:"href,omitempty"`
		Name   string `json:"name,omitempty"`
		Interval string `json:"interval,omitempty"`
		Type string `json:"alert_type,omitempty"`
		Email []string `json:"alert_email,omitempty"`
		Tags  []string `json:"tags,omitempty"`
		Notes string `json:"notes,omitempty"`
	}

	var a Alias

	err := json.Unmarshal(data, &a)
	if err != nil {
		return err
	}

	email := []types.String{}
	tags  := []types.String{}
	for _, s := range a.Email {
		email = append(email, types.StringValue(s))
	}
	for _, s := range a.Tags {
		tags = append(tags, types.StringValue(s))
	}

	m.Id = types.StringValue(a.Token)
	m.Href   = types.StringValue(a.Href)
	m.Name  = types.StringValue(a.Name)
	m.Type = types.StringValue(a.Type)
	m.Interval = types.StringValue(a.Interval)
	m.Email = email
	m.Tags  = tags
	m.Notes = types.StringValue(a.Notes)

	return nil
}

func (m *snitchResourceModel) MarshalJSON() ([]byte, error) {
	tags := []string{}
	for _, t := range m.Tags {
		tags = append(tags, t.ValueString())
	}
	email := []string{}
	for _, t := range m.Email {
		email = append(email, t.ValueString())
	}

	return json.Marshal(&struct {
		Token  string `json:"token,omitempty"`
		Href    string `json:"href,omitempty"`
		Name   string `json:"name,omitempty"`
		Interval string `json:"interval,omitempty"`
		Type string `json:"alert_type,omitempty"`
		Email []string `json:"alert_email,omitempty"`
		Tags  []string `json:"tags,omitempty"`
		Notes string `json:"notes,omitempty"`
	}{
		Token: m.Id.ValueString(),
		Href:   m.Href.ValueString(),
		Name:  m.Name.ValueString(),
		Interval: m.Interval.ValueString(),
		Type: m.Type.ValueString(),
		Email: email,
		Tags: tags,
		Notes: m.Notes.ValueString(),
	})
}

// Metadata returns the resource type name.
func (r *snitchResource) Metadata(_ context.Context, req resource.MetadataRequest, resp *resource.MetadataResponse) {
	resp.TypeName = req.ProviderTypeName + "_snitch"
}

// Schema defines the schema for the resource.
func (r *snitchResource) Schema(_ context.Context, _ resource.SchemaRequest, resp *resource.SchemaResponse) {
	resp.Schema = schema.Schema{
		Attributes: map[string]schema.Attribute{
			"id": schema.StringAttribute{
				Computed: true,
				// Plan modifier
				PlanModifiers: []planmodifier.String{stringplanmodifier.UseStateForUnknown()},
			},
			"name": schema.StringAttribute{
				Required: true,
			},
			"notes": schema.StringAttribute{
				Optional: true,
				//Default: "Managed by Terraform",
			},
			"alert_type": schema.StringAttribute{
				Optional: true,
				//Default: "basic",
			},
			"alert_email": schema.SetAttribute{
				Optional:    true,
				ElementType: types.StringType,
			},
			"tags": schema.SetAttribute{
				Optional:    true,
				ElementType: types.StringType,
			},
			"interval": schema.StringAttribute{
				Optional: true,
				//Default: "daily",
				PlanModifiers: []planmodifier.String{stringplanmodifier.UseStateForUnknown()},
			},
			"href": schema.StringAttribute{
				Computed: true,
				PlanModifiers: []planmodifier.String{stringplanmodifier.UseStateForUnknown()},
			},
		},
	}
}

// Configure adds the provider configured client to the resource.
func (r *snitchResource) Configure(ctx context.Context, req resource.ConfigureRequest, _ *resource.ConfigureResponse) {
	if req.ProviderData == nil {
		return
	}

	r.client = req.ProviderData.(*DMSnitchClient)
	tflog.Debug(ctx, "Creating DMSnitch client")
}
