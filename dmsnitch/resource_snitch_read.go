package dmsnitch

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-log/tflog"
)

// Read refreshes the Terraform state with the latest data.
func (r *snitchResource) Read(ctx context.Context, req resource.ReadRequest, resp *resource.ReadResponse) {
	// Get current state
	var state snitchResourceModel
	diags := req.State.Get(ctx, &state)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	dresp, err := r.client.Get(fmt.Sprintf("snitches/%s", state.Id.ValueString()))
	if err != nil {
		resp.Diagnostics.AddError(
			"Error reading snitch",
			"Could not read snitch, unexpected error: "+err.Error(),
		)
		return
	}

	
	body, err := ioutil.ReadAll(dresp.Body)
	if err != nil {
		resp.Diagnostics.AddError(
			"Error reading snitch",
			"Could not read snitch, unexpected error: "+err.Error(),
		)
		return
	}

	err = json.Unmarshal(body, &state)
	if err != nil {
		resp.Diagnostics.AddError(
			"Error reading snitch",
			"Could not unmarshal snitch, unexpected error: "+err.Error(),
		)
		return
	}

	tflog.Debug(ctx, fmt.Sprintf("DMS/ReadSnitch-State: %+v", state))

	diags = resp.State.Set(ctx, &state)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}
}
