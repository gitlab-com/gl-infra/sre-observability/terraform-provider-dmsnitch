package dmsnitch

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-log/tflog"
)

// Update updates the resource and sets the updated Terraform state on success.
func (r *snitchResource) Update(ctx context.Context, req resource.UpdateRequest, resp *resource.UpdateResponse) {
	// Retrieve values from plan
	var plan snitchResourceModel
	var state snitchResourceModel
	diags := req.Plan.Get(ctx, &plan)
	statediags := req.State.Get(ctx, &state)

	tflog.Debug(ctx, fmt.Sprintf("DMS/UpdateSnitch-Plan: %+v", plan))
	tflog.Debug(ctx, fmt.Sprintf("DMS/UpdateSnitch-State: %+v", state))

	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	resp.Diagnostics.Append(statediags...)
	if resp.Diagnostics.HasError() {
		return
	}

	bytedata, err := json.Marshal(&plan)
	if err != nil {
		resp.Diagnostics.AddError(
			"Error creating snitch",
			"Could not create snitch, unexpected error, while Marshaling..: "+err.Error(),
		)
		return
	}

	_, err = r.client.Patch(fmt.Sprintf("snitches/%s", state.Id.ValueString()), bytes.NewBuffer(bytedata))
	if err != nil {
		resp.Diagnostics.AddError(
			"Error updating snitch",
			"Could not update snitch, unexpected error, while Patching...: "+err.Error(),
		)
		return
	}

	dresp, err := r.client.Get(fmt.Sprintf("snitches/%s", state.Id.ValueString()))
	if err != nil {
		resp.Diagnostics.AddError(
			"Error reading snitch",
			"Could not read snitch, unexpected error: "+err.Error(),
		)
		return
	}

	body, err := ioutil.ReadAll(dresp.Body)
	if err != nil {
		resp.Diagnostics.AddError(
			"Error reading snitch",
			"Could not read snitch, unexpected error: "+err.Error(),
		)
		return
	}

	err = json.Unmarshal(body, &plan)
	if err != nil {
		resp.Diagnostics.AddError(
			"Error reading snitch",
			"Could not unmarshal snitch, unexpected error: "+err.Error(),
		)
		return
	}

	diags = resp.State.Set(ctx, &plan)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	return
}
