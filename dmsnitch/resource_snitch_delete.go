package dmsnitch

import (
	"context"
	"fmt"
	"github.com/hashicorp/terraform-plugin-framework/resource"
)

// Delete deletes the resource and removes the Terraform state on success.
func (r *snitchResource) Delete(ctx context.Context, req resource.DeleteRequest, resp *resource.DeleteResponse) {
	var state snitchResourceModel
	diags := req.State.Get(ctx, &state)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	_, err := r.client.Delete(fmt.Sprintf("snitches/%s", state.Id.ValueString()))
	if err != nil {
		resp.Diagnostics.AddError(
			"Error deleting snitch",
			"Could not delete snitch from DMS api, unexpected error: "+err.Error(),
		)
		return
	}
}
