package dmsnitch

import (
    "context"

    "github.com/hashicorp/terraform-plugin-framework/datasource"
    "github.com/hashicorp/terraform-plugin-framework/datasource/schema"
	"github.com/hashicorp/terraform-plugin-framework/types"
)


type snitchDataSourceModel struct {
	ID       types.String   `tfsdk:"token"`
	Url      types.String   `tfsdk:"href"`
	Name     types.String   `tfsdk:"name"`
	Interval types.String   `tfsdk:"interval"`
	Type     types.String   `tfsdk:"alert_type"`
	Email    []types.String `tfsdk:"alert_email"`
	Notes    types.String   `tfsdk:"notes"`
	Tags     []types.String `tfsdk:"tags"`
}


// Ensure the implementation satisfies the expected interfaces.
var (
    _ datasource.DataSource = &snitchDataSource{}
)

// NewsnitchDataSource is a helper function to simplify the provider implementation.
func NewSnitchDataSource() datasource.DataSource {
    return &snitchDataSource{}
}

// snitchDataSource is the data source implementation.
type snitchDataSource struct{}

// Metadata returns the data source type name.
func (d *snitchDataSource) Metadata(_ context.Context, req datasource.MetadataRequest, resp *datasource.MetadataResponse) {
    resp.TypeName = req.ProviderTypeName + "_coffees"
}

// Schema defines the schema for the data source.
func (d *snitchDataSource) Schema(_ context.Context, _ datasource.SchemaRequest, resp *datasource.SchemaResponse) {
    resp.Schema = schema.Schema{}
}

// Read refreshes the Terraform state with the latest data.
func (d *snitchDataSource) Read(ctx context.Context, req datasource.ReadRequest, resp *datasource.ReadResponse) {
}