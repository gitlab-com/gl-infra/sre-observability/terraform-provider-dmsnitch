{ pkgs }:
let 
in
pkgs.mkShell {
  name = "terraform-provider-dmsnitch";
  buildInputs = with pkgs; [
    go
    tfplugindocs
  ];
}
