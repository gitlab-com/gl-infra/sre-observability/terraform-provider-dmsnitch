GOBIN = $(shell pwd)/bin
PROVIDER_SRC_DIR := ./dmsnitch/...
TERRAFORM_PLUGIN_DIR ?= ~/.terraform.d/plugins/gitlab.local/x/gitlab/99.99.99
TERRAFORM_PLATFORM_DIR ?= darwin_amd64

build: ## Build the provider binary.
	go mod tidy
	GOBIN=$(GOBIN) go install


test: ## Run unit tests.
	go test $(TESTARGS) $(PROVIDER_SRC_DIR)
